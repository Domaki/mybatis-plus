package com.example.mp;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.mp.dao.StudentDao;
import com.example.mp.pojo.Student;
import com.example.mp.service.StudentService;
import com.example.mp.utils.MD5Util;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
class MpApplicationTests {
    @Autowired
    StudentDao studentDao;
    @Autowired
    StudentService studentService;

    @Test
    void contextLoads() {
        Student student = studentDao.selectById(1);
        System.out.println(student);
        Page<Student> page = new Page<>(0,5);
        QueryWrapper<Student> wrapper = new QueryWrapper<Student>().eq("sex", "男").or().like("name", "阿");
        studentDao.selectPage(page, wrapper);
        page.getRecords().forEach(System.out::println);
        System.out.println(page.getTotal());
    }
    @Test
    void test(){
        int i = studentDao.deleteById(241);
        System.out.println(i);
    }
    //乐观锁
    @Test
    void test1(){
        Student student = studentDao.selectById(243);
        System.out.println(student);
        student.setName("张三啊3");
        int i = studentDao.updateById(student);
        System.out.println(i);
    }
    @Test
    void test2(){
        //lambda
        List<Student> students = studentService.list(new QueryWrapper<Student>());
        List<Student> collect = students.stream().map(student -> {
            Student student1 = new Student();
            student1.setName(student.getName());
            student1.setPassword(student.getPassword());
            student1.setAge(student.getAge());
            return student1;
        }).filter(student -> student.getAge()>100).collect(Collectors.toList());
        System.out.println(collect);
    }

    @Test
    void test3(){
        String s = MD5Util.md5slat("123456");
        System.out.println(s);
    }

}
