package com.example.mp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.mp.pojo.Student;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TestDao extends BaseMapper<Student> {
}
