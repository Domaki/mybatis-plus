package com.example.mp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.mp.pojo.Student;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface StudentDao extends BaseMapper<Student> {
    public Student login(String loginName, String password);

    public List<Student> getAll();

    public Student getInfoById(Integer id);

    public int delete(Integer id);

    public void add(Student student);

    public void updateInfo(Student student);

    public List<Student> search(String keys);
}
