package com.example.mp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.mp.pojo.User;

public interface UserDao extends BaseMapper<User> {
}
