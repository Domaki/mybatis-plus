package com.example.mp.config;

/**
 * @author: xuke
 * @description: ILettuceRedisKeyManager
 * @Date : 2021/2/1
 */
public interface ILettuceRedisKeyManager {

    // 缓存文章导航栏的分类的key
    String BBS_CATEGORY_KEY = "findBbsCategorys";
    String RESUME_CATEGORY_KEY = "findResumeCategorys";
    String WEBTEMPLATE_CATEGORY_KEY = "findWebTemplateCategorys";
    // 缓存文章导航栏的分类的key含有统计
    String BBS_CATEGORY_KEY_COUNT = "findBbsCategorysCount";
    // 缓存文章导航栏的分类的key
    String APP_NAVS_KEY = "findAppnavs";
    // 缓存文章导航栏的分类的key
    String SELECT_COURSE_KEY = "selectCourses";
    // 缓存文章导航栏的分类的key
    String COURSE_SUBJECT_KEY = "findCourseSubject";
    // 缓存价格
    String SYSTEM_PRICE_KEY = "ksd_systemprice";
}
