package com.example.mp.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.apache.ibatis.reflection.MetaObject;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Date;
@Configuration
@MapperScan({"com.example.mp.mapper"})
public class MybatisPlusConfig {

    @Bean
    public ISqlInjector sqlInjector() {
        return new DefaultSqlInjector();
    }
        /**
         * 初始化分页插件
         * 乐观锁
         */
        @Bean
        public MybatisPlusInterceptor mybatisPlusInterceptor() {
            MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
            interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
            interceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
            return interceptor;
        }


    /**
         * 初始化公共字段自动填充功能
         */
        @Component
        public class MetaObjectHandlerConfiguration implements MetaObjectHandler {
            @Override
            public void insertFill(MetaObject metaObject) {
                Date today = new Date();
                this.setFieldValByName("createdDate", today, metaObject);
                this.setFieldValByName("lastUpdateDate", today, metaObject);
                this.setFieldValByName("deleted",0,metaObject);
                this.setFieldValByName("version",0,metaObject);
            }

            @Override
            public void updateFill(MetaObject metaObject) {
                this.setFieldValByName("lastUpdateDate", new Date(), metaObject);
            }
        }
    }
