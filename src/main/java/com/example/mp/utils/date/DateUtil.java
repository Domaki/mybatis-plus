package com.example.mp.utils.date;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * @author: xuke
 * @description: DateUtil
 * @Date : 2021/1/30
 */
public class DateUtil {

    /**
     * 获取某天的时间
     *
     * @param index 为正表示当前时间加天数，为负表示当前时间减天数
     * @return String
     */
    public static String getTimeDay(int index) {
        TimeZone tz = TimeZone.getTimeZone("Asia/Shanghai");
        TimeZone.setDefault(tz);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        calendar.add(Calendar.DAY_OF_MONTH, index);
        String date = fmt.format(calendar.getTime());
        return date;
    }

    /**
     * 获取某天的时间
     *
     * @param index 为正表示当前时间加天数，为负表示当前时间减天数
     * @return String
     */
    public static String getTimeMonth(int monthindex) {
        TimeZone tz = TimeZone.getTimeZone("Asia/Shanghai");
        TimeZone.setDefault(tz);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM");
        calendar.add(Calendar.MONTH, monthindex);
        String date = fmt.format(calendar.getTime());
        return date;
    }

    public static void main(String[] args){
        System.out.println(getTimeMonth(0));
        System.out.println(getTimeMonth(-1));
    }

}
