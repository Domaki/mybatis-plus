package com.example.mp.vo;

import lombok.Data;

@Data
public class UserVo {
    private Integer id;
    private String username;
    private String password;
    private String token;
    //签名
    private String sign;
    //刷新token
    private String refreshToken;
}
