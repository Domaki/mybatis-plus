package com.example.mp.vo;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2021/8/3 14:27
 */
public class AuthResponse {
    private UserVo userVo;
    private Long code;

    // 调用过程中，保持一种调用。直接用类去调用。
    private AuthResponse() {

    }

    /**
     * @return com.kuangstudy.common.R
     * @Author xuke
     * @Description 成功返回
     * @Date 21:55 2021/6/23
     * @Param []
     **/
    public static AuthResponse success(UserVo userVo, Long code) {
        AuthResponse authResponse = new AuthResponse();
        authResponse.setUserVo(userVo);
        authResponse.setCode(code);
        return authResponse;
    }

    public static AuthResponse code(Long code) {
        AuthResponse authResponse = new AuthResponse();
        authResponse.setUserVo(null);
        authResponse.setCode(code);
        return authResponse;
    }

    public UserVo getUserVo() {
        return userVo;
    }

    public void setUserVo(UserVo userVo) {
        this.userVo = userVo;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }
}
