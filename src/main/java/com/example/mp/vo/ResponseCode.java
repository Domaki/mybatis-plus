package com.example.mp.vo;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2021/8/3 14:27
 */
public class ResponseCode {

    // 登录成 1L
    public static final Long SUCCESS = 1L;
    // 密码有误
    public static final Long INCORRECT_PASSWORD = 1000L;
    // 用户找不到
    public static final Long USER_NOT_FOUND = 1001L;
}
