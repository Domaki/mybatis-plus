package com.example.mp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.mp.dao.StudentDao;
import com.example.mp.pojo.Student;

import java.util.List;

public interface StudentService extends IService<Student>{
    public Student login(String loginName, String password);

    public List<Student> getAll();

    public Student getInfoById(Integer id);

    public int delete(Integer id);

    public void add(Student student);
    //乐观锁
    public void updateInfo(Student student);

    public List<Student> search(String keys);
}
