package com.example.mp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.mp.pojo.User;

public interface UserService extends IService<User> {
    public User  login(String username,String password);
   // User getByUsername();
}
