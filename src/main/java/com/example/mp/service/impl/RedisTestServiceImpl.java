package com.example.mp.service.impl;

import com.example.mp.service.RedisTestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class RedisTestServiceImpl implements RedisTestService {
    @Autowired
    RedisTemplate redisTemplate;

    public static String RANDON_SET_KEY="random:set";
    @Override
    public void InitData(){
        boolean flag = redisTemplate.hasKey(RANDON_SET_KEY);
        if (!flag){
            List<String> initDataList = initDataList();
            initDataList.forEach(data->this.redisTemplate.opsForSet().add(RANDON_SET_KEY,data));
        }

    }

//    @PostConstruct
    public static String REDPACKAGE="RedPackage:";
    @Override
    //初始化红包

    public void red(Integer total,Integer person,String qq) {
        //计算出红包的数量存入redis的list中
        Integer[] integers = this.splitRedPackage(total, person);

        redisTemplate.opsForList().leftPushAll(REDPACKAGE+qq, integers);
        redisTemplate.expire(REDPACKAGE+qq, 3, TimeUnit.MINUTES);

        String redId = UUID.randomUUID().toString().substring(0, 3);
        redisTemplate.opsForHash().put(REDPACKAGE+"redId:"+redId,"#","#");
        redisTemplate.expire(REDPACKAGE+"redId:"+redId,3, TimeUnit.MINUTES);
        System.out.println(redId);
    }

    private Integer[] splitRedPackage(Integer total, Integer person) {
        Integer[] array = new Integer[person];
        int use=0;

        Random random = new Random();
        for (int i = 0; i < person; i++) {
            if (i==person-1){
                array[i]=total-use;

            }else {
                int avg=(total-use)*2/(person-i);
                array[i]=1+random.nextInt(avg-1);
            }
            use = use +array[i];
        }
        return array;

    }

/*    public static void main(String[] args) {
        Integer[] integers = new RedisTestServiceImpl().splitRedPackage(1000, 50);
        for (int i = 0; i < integers.length; i++) {
            System.out.println(integers[i]);
        }
    }*/
    //每次抽调用一次方法
    public static double getRandomMoney(double total, Integer person) {
        // remainSize 剩余的红包数量
        // remainMoney 剩余的钱
        if (person == 1) {
            person--;
            return (double) Math.round(total * 100) / 100;
        }
        Random r = new Random();
        double min = 0.01; //
        double max = total / person * 2;
        if (max > 6) {
            max = 6;
        }
        double money = r.nextDouble() * max;
        money = money <= min ? 0.01 : money;
        money = Math.floor(money * 100) / 100;
        person--;
        total -= money;
        return money;
    }


    public List<String> initDataList(){
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add("100#"+i);
        }

        for (int i = 0; i < 20; i++) {
            list.add("50#"+i);
        }
        for (int i = 0; i < 60; i++) {
            list.add("10#"+i);
        }
        for (int i = 0; i < 10; i++) {
            list.add("0#"+i);
        }
        return list;
    }


}
