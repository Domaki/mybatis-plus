package com.example.mp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.mp.common.exception.ValidationException;
import com.example.mp.dao.StudentDao;
import com.example.mp.dao.UserDao;
import com.example.mp.pojo.Student;
import com.example.mp.pojo.User;
import com.example.mp.service.StudentService;
import com.example.mp.service.UserService;
import com.example.mp.utils.MD5Util;
import com.example.mp.utils.R;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {
    @Override
    public User login(String password, String username) {
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<User>().eq("username",username);
        User user = this.baseMapper.selectOne(userQueryWrapper);
        String password1=user.getPassword();

        if (password1.equals( MD5Util.md5slat(password))){
            return user;
        }else {
            throw new ValidationException(403,"用户名或密码错误");
        }
    }

}
