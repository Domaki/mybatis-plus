package com.example.mp.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.mp.dao.StudentDao;
import com.example.mp.pojo.Student;
import com.example.mp.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class StudentServiceImpl  extends ServiceImpl<StudentDao,Student> implements StudentService{

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private StudentDao studentDao;

    @Override
    public Student login(String loginName, String password) {
        return studentDao.login(loginName,password);
    }

    @Override
    public List<Student> getAll() {

        return studentDao.getAll();
    }

    @Override
    public Student getInfoById(Integer id) {
        return studentDao.getInfoById(id);
    }

    @Override
    public int delete(Integer id) {
        return studentDao.deleteById(id);
    }

    @Override
    public void add(Student student) {
        redisTemplate.opsForValue().set("student",student);
        studentDao.add(student);
    }
    @Transactional
    //异常捕获后不会回滚 此时在service层
    @Override
    public void updateInfo(Student student) {
        Student student1 = studentDao.selectById(student.getId());
        student.setVersion(student1.getVersion());
        studentDao.update(student,new QueryWrapper<Student>().eq("id",student.getId()));
    }

    @Override
    public List<Student> search(String keys) {
        return studentDao.search(keys);
    }
}
