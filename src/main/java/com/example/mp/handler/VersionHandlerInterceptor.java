package com.example.mp.handler;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author xuke
 * @description 资源文件版本控制和资源文件路径的问题
 * @params
 * @date 2021/2/1 14:46
 */
public class VersionHandlerInterceptor implements HandlerInterceptor {

    @Value("${ksd.version}")
    private String version;
    @Value("${ksd.filepath}")
    private String filepath;
    @Value("${spring.profiles.active}")
    private String profile;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        request.setAttribute("version", version);
        request.setAttribute("filepath", filepath);
        request.setAttribute("proflie", profile);
        response.setHeader("x-frame-options", "SAMEORIGIN");
        response.setHeader("Set-Cookie", "SameSite=None; Secure");
        return true;
    }


    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        //UserThreadLocal.remove();
    }

    // 此处两个方法，一定都是要preHandle方法返回为true的时候执行
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        //UserThreadLocal.remove();
    }
}