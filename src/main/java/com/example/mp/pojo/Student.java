package com.example.mp.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student implements Serializable {
    @TableId(type = IdType.AUTO)
    private Integer id;

    private String name;

    private Integer age;

    private String loginName;

    private String phoneNum;

    private String password;

    private String sex;
    //1表示没被删除 0表示已被逻辑删除
    @TableField(fill = FieldFill.INSERT)
    @TableLogic
    private Integer deleted;

    @TableField( fill = FieldFill.INSERT)
    private Date createdDate;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date lastUpdateDate;

    //乐观锁
    @TableField(fill = FieldFill.INSERT)
    @Version
    private Integer version;
}
