package com.example.mp.common;

/**
 * @description: 统一返回的常量类
 * 对内修改开放，对外修改关闭---枚举
 * @author: xuke
 * @time: 2021/6/23 22:12
 */
public enum ResponseEnum {

    SUCCESS(200,"成功!"),

    USER_REG_USER_PASSWORD_CODE(401,"用户名和密码错误"),
    USER_REG_USER_PASSWORD_CONFIRM(402,"密码和确认密码不一致"),
    ORDER_FAIL(601,"订单失败"),
    ORDER_MESSAGE_FAIL(602,"订单发送消息失败") ;

    private Integer code;
    private String message;

    ResponseEnum(Integer code,String mesage){
        this.code = code;
        this.message =mesage;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
