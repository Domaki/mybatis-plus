package com.example.mp.common.limit.annotation;

import java.lang.annotation.*;

/**
 * @author 飞哥
 * @Title: 学相伴出品
 * @Description: 我们有一个学习网站：https://www.kuangstudy.com
 * @date 2021/7/7 11:38
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AccessIpLimiter {
    // 每timeout限制请求的个数
    int limit() default 1;

    // 时间，单位默认是秒
    int timeout() default 1;

    // 缓存的key
    String key() default "";
}
