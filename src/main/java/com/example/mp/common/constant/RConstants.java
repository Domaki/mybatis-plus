package com.example.mp.common.constant;

/**
 * @description: 统一返回的常量类
 * @author: xuke
 * @time: 2021/6/23 22:12
 */
public class RConstants {

    // 用户名和密码错误信息和状态
    public static final Integer USER_REG_USER_PASSWORD_CODE = 401;
    public static final String USER_REG_USER_PASSWORD_ERROR = "用户名和密码错误!";

    // 密码和确认密码错误信息和状态
    public static final Integer USER_REG_USER_PASSWORD_CONFIRM_CODE = 402;
    public static final String USER_REG_USER_PASSWORD_CONFIRM_ERROR = "密码和确认密码不一致!";

}
