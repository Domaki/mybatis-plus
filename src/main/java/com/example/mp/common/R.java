package com.example.mp.common;

/**
 * @description:
 * @author: xuke
 * @time: 2021/6/23 21:47
 */


public class R {

    // 返回的编号
    private Integer code;
    // 返回的数据,数据类型N中，
    private Object data;
    // 返回的信息
    private String message;

    // 调用过程中，保持一种调用。直接用类去调用。
    private R() {

    }

    /**
     * @return com.kuangstudy.common.R
     * @Author xuke
     * @Description 成功返回
     * @Date 21:55 2021/6/23
     * @Param []
     **/
    public static R success(Object data, String message) {
        R r = new R();
        r.setCode(ResponseEnum.SUCCESS.getCode());
        r.setData(data);
        r.setMessage(message == null ? ResponseEnum.SUCCESS.getMessage() : message);
        return r;
    }

    /**
     * @return com.kuangstudy.common.R
     * @Author xuke
     * @Description 成功返回
     * @Date 21:55 2021/6/23
     * @Param []
     **/
    public static R success(Object data) {
        return success(data, null);
    }


    /**
     * @return com.kuangstudy.common.R
     * @Author xuke
     * @Description
     * @Date 22:03 2021/6/23
     * @Param [code 失败的状态, message 失败的原因]
     **/
    public static R fail(Integer code, String message) {
        R r = new R();
        r.setCode(code);
        r.setData(null);
        r.setMessage(message);
        return r;
    }

    /**
     * @return com.kuangstudy.common.R
     * @Author xuke
     * @Description
     * @Date 22:03 2021/6/23
     * @Param [code 失败的状态, message 失败的原因]
     **/
    public static R fail(ResponseEnum responseEnum) {
        R r = new R();
        r.setCode(responseEnum.getCode());
        r.setData(null);
        r.setMessage(responseEnum.getMessage());
        return r;
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

