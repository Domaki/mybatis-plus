package com.example.mp.common;

/**
 * @Author xueke
 * @Date 2020/5/25 14:52
 */
public class UserThreadLocal {

    private static ThreadLocal<Object> userDoThreadLocal = new ThreadLocal<>();


    public static void put(Object userDo) {
        userDoThreadLocal.set(userDo);
    }

    public static Object get() {
        return userDoThreadLocal.get();
    }

    public static void remove() {
        userDoThreadLocal.remove();
    }
}
