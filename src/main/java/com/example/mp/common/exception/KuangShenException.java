package com.example.mp.common.exception;

public class KuangShenException extends RuntimeException {
    private Integer code;

    /**
     * 接受状态码和消息
     * @param code
     * @param message
     */
    public KuangShenException(Integer code, String message) {
        super(message);
        this.code=code;
    }

    /**
     * 接收枚举类型
     * @param resultCodeEnum
     */
    public KuangShenException(ResultCodeEnum resultCodeEnum) {
        super(resultCodeEnum.getMessage());
        this.code = resultCodeEnum.getCode();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}