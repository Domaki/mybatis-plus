package com.example.mp.common;

/**
 * @Author xuke
 * @Description 会话常量类
 * @Date 1:51 2020/9/26
 * @Param
 * @return
 **/
public class SessionContants {

    public static final String LOGIN_USER = "loginUser";
    public static final String LOGIN_USER_COUNT_MAP = "countMap";


}