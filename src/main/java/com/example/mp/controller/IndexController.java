package com.example.mp.controller;

import com.alibaba.druid.util.StringUtils;
import com.example.mp.service.RedisTestService;
import com.example.mp.service.impl.RedisTestServiceImpl;
import com.example.mp.utils.R;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.example.mp.service.impl.RedisTestServiceImpl.REDPACKAGE;

@RestController
public class IndexController {
    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    RedisTestService redisTestService;

    public static int count = 0;
    @ApiOperation(value = "自增一")
    @GetMapping("/count")
    public R count(){

        if (redisTemplate.opsForValue().get("count1")!=null){
            redisTemplate.opsForValue().set("count1",count);
        }count++;
        redisTemplate.opsForValue().increment("count1");
        System.out.println(count);
        return R.ok().put("count",count);
    }


    @ApiOperation(value = "抽奖")
    @GetMapping("/random/card")
    public R randomData(){
        String result =null;
        redisTestService.InitData();
        String o = (String) redisTemplate.opsForSet().randomMember(RedisTestServiceImpl.RANDON_SET_KEY);
        if (!StringUtils.isEmpty(o)){
            int i = o.indexOf("#");
            int coin = Integer.valueOf(o.substring(0,i));
            if (coin==0){
                result ="谢谢参与";
            }else if (coin==10){
                result = "获得十个硬币";
            }else if (coin==50){
                result = "获得五十个硬币";
            }else if (coin==100){
                result = "获得一百个硬币";
            }
        }
        System.out.println(o);
        return R.ok().put("result",result);
    }
    @GetMapping("/sendRed")
    @ApiOperation(value = "发送红包")
    public R sendRed(Integer total,Integer person,String qq){
        redisTestService.red(total,person,qq);

        System.out.println(redisTemplate.opsForList().range(REDPACKAGE+qq, 0, -1));
        return R.ok().put("msg","红包分配成功");
    }

    @GetMapping("/receiveRed")
    @ApiOperation(value = "抢红包")
    public R receive(Integer userId,String qq,String redId){
        if (redisTemplate.hasKey(REDPACKAGE+"redId:"+redId)){

            Boolean hasKey = redisTemplate.opsForHash().hasKey(REDPACKAGE + "redId:" + redId, userId);
            if (!hasKey){
                Integer red = (Integer) redisTemplate.opsForList().leftPop(REDPACKAGE+qq);
                if (red!=null){
                    redisTemplate.opsForHash().put(REDPACKAGE+"redId:"+redId,userId,red);
                    System.out.println(redisTemplate.opsForList().range(REDPACKAGE+qq,0 ,-1));
                    return R.ok().put("msg","您抢到了"+red+"元的红包");
                }

            }else {
                return R.error("红包已被领完或是您已强过红包");
            }
        }
        return R.error(501,"系统错误,指定的红包找不到");
    }
}
