package com.example.mp.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.mp.dao.StudentDao;
import com.example.mp.pojo.Student;
import com.example.mp.service.StudentService;
import com.example.mp.utils.R;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
@Slf4j
@RestController
@RequestMapping("/student")
public class StudentController {
    @Autowired
    StudentDao studentDao;

    @Autowired
    StudentService studentService;
    /**
     * 分页查询
     * 1 0
     * 2 5
     * 3 10
     */
    @GetMapping("/getInfo/{id}")
    public R getOne(@PathVariable("id") Integer id){
        Student student = studentDao.selectById(id);
        return R.ok().put("data",student);
    }
    @GetMapping("/getAll")
    public R all(){
        List<Student> students = studentDao.selectList(new QueryWrapper<Student>());
        for (Student student : students) {
            System.out.println(student);
        }
        return R.ok().put("data",students);
    }
    @ApiOperation(value = "分页查询")
    @GetMapping("/select")
    public R selectPage(@RequestParam("pageNum") Integer pageNum){
        Integer pageSize=5;
        Page<Student> studentPage = new Page<Student>(pageNum,pageSize);
        log.info("分页查询开启");
        studentDao.selectPage(studentPage, new QueryWrapper<Student>());
        studentPage.getRecords().forEach(System.out::println);
        return R.ok().put("data",studentPage.getRecords());
    }
    @ExceptionHandler
    @PostMapping("/update")
    public R update(@RequestBody Student student){
        studentService.updateInfo(student);
        return R.ok().put("msg","更新成功");
    }

    @PostMapping("/insert")
    public R insert(@RequestBody Student student){
        int insert = studentDao.insert(student);
        return R.ok().put("msg",insert);
    }
    @GetMapping("/delete/{id}")
    public R delete(@PathVariable("id") Integer id){
        studentService.delete(id);
        return R.ok().put("msg","删除成功");
    }
}
