package com.example.mp.controller;

import com.example.mp.common.exception.ValidationException;
import com.example.mp.handler.jwt.JwtService;
import com.example.mp.pojo.User;
import com.example.mp.service.UserService;
import com.example.mp.utils.R;
import com.example.mp.vo.AuthResponse;
import com.example.mp.vo.ResponseCode;
import com.example.mp.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;
import java.util.concurrent.TimeUnit;


@RestController
public class LoginController {
    @Autowired
    private UserService userService;
    @Autowired
    private JwtService jwtService;
    @Autowired
    private RedisTemplate redisTemplate;

    //多次登陆会产生多个不同的token，使用uuid不太友好，应使用自定义唯一的key作为refreshToken。防止用户登陆产生多可用的token
    @PostMapping("/login")
    public AuthResponse login(String password, String username){
        if (username==null || password==null){
            throw new ValidationException(403,"用户名或密码错误");

        }
        UserVo userVo = new UserVo();
        User user = userService.login(password, username);
        userVo.setId(user.getId());
        userVo.setUsername(user.getUsername());
        userVo.setPassword(user.getPassword());
        String token = jwtService.token(userVo);
        userVo.setToken(token);
        userVo.setRefreshToken(UUID.randomUUID().toString());
        redisTemplate.opsForValue().set(userVo.getRefreshToken(),userVo,JwtService.TOKEN_EXPIRE_TIME, TimeUnit.MILLISECONDS);
        return AuthResponse.success(userVo, ResponseCode.SUCCESS);


    }
    @PostMapping("/refresh")
    public AuthResponse refresh(String refreshToken){
        UserVo userVo = (UserVo) redisTemplate.opsForValue().get(refreshToken);
        if (userVo==null){
            return AuthResponse.code(ResponseCode.USER_NOT_FOUND);
        }
        String token = jwtService.token(userVo);
        userVo.setToken(token);
        userVo.setRefreshToken(UUID.randomUUID().toString());
        redisTemplate.delete(refreshToken);
        redisTemplate.opsForValue().set(userVo.getRefreshToken(),userVo,JwtService.TOKEN_EXPIRE_TIME, TimeUnit.MILLISECONDS);
        return AuthResponse.success(userVo,ResponseCode.SUCCESS);
    }

    @GetMapping("/verifyUsername")
    public AuthResponse verify(String username, String token) {
        return jwtService.verify(token, username);
    }


    @GetMapping("/verifyId")
    public AuthResponse verifyuserid(Integer id, String token) {
        return jwtService.verifyUserId(token, id);
    }

}
